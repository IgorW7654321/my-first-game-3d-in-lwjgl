package scene;

import org.lwjgl.util.vector.Vector3f;

import animatedModel.AnimatedModel;


public class Scene {

	private final ICamera camera;

	private final AnimatedModel animatedModel;

	private Vector3f lightDirection = new Vector3f(0, -1, 0);

	public Scene(AnimatedModel model, ICamera cam) {
		this.animatedModel = model;
		this.camera = cam;
	}


	public ICamera getCamera() {
		return camera;
	}

	public AnimatedModel getAnimatedModel() {
		return animatedModel;
	}


	public Vector3f getLightDirection() {
		return lightDirection;
	}

	public void setLightDirection(Vector3f lightDir) {
		this.lightDirection.set(lightDir);
	}

}
