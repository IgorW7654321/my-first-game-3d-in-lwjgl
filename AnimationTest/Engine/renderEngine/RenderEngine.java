package renderEngine;

import renderer.AnimatedModelRenderer;
import scene.Scene;
import skybox.SkyboxRenderer;
import utils.DisplayManager;


public class RenderEngine {

	private MasterRenderer renderer;

	private RenderEngine(MasterRenderer renderer) {
		this.renderer = renderer;
	}


	public void update() {
		DisplayManager.update();
	}

	public void renderScene(Scene scene) {
		renderer.renderScene(scene);
	}


	public void close() {
		renderer.cleanUp();
		DisplayManager.closeDisplay();
	}


	public static RenderEngine init() {
		DisplayManager.createDisplay();
		SkyboxRenderer skyRenderer = new SkyboxRenderer();
		AnimatedModelRenderer entityRenderer = new AnimatedModelRenderer();
		MasterRenderer renderer = new MasterRenderer(entityRenderer, skyRenderer);
		return new RenderEngine(renderer);
	}

}
