package renderer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

import animatedModel.AnimatedModel;
import scene.ICamera;
import utils.OpenGlUtils;


public class AnimatedModelRenderer {

	private AnimatedModelShader shader;


	public AnimatedModelRenderer() {
		this.shader = new AnimatedModelShader();
	}


	public void render(AnimatedModel entity, ICamera camera, Vector3f lightDir) {
		prepare(camera, lightDir);
		entity.getTexture().bindToUnit(0);
		entity.getModel().bind(0, 1, 2, 3, 4);
		shader.jointTransforms.loadMatrixArray(entity.getJointTransforms());
		GL11.glDrawElements(GL11.GL_TRIANGLES, entity.getModel().getIndexCount(), GL11.GL_UNSIGNED_INT, 0);
		entity.getModel().unbind(0, 1, 2, 3, 4);
		finish();
	}


	public void cleanUp() {
		shader.cleanUp();
	}


	private void prepare(ICamera camera, Vector3f lightDir) {
		shader.start();
		shader.projectionViewMatrix.loadMatrix(camera.getProjectionViewMatrix());
		shader.lightDirection.loadVec3(lightDir);
		OpenGlUtils.antialias(true);
		OpenGlUtils.disableBlending();
		OpenGlUtils.enableDepthTesting(true);
	}

	
	private void finish() {
		shader.stop();
	}

}
