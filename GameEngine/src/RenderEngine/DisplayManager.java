package renderEngine;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.PixelFormat;
import org.lwjgl.util.vector.Vector2f;

import fontMeshCreator.FontType;
import fontMeshCreator.GUIText;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

import javax.management.timer.Timer;


public class DisplayManager{
	
	private static final int WIDTH = 1280;
	private static final int HEIGHT = 720;
	private static final int FPS_CAP = 120;
	private static final int DEPHTS = 24;
	public static final int SAMPLES = 4;
	
	private static long lastFrameTime;
	private static long currentFrameTime;
	private static float delta;
	private static float refresh;
	private static float totalRefresh;
	private static int i = 0;
	private static long y = 0;
	public static int MasterRefresh;
	public static List<GUIText> texted = new ArrayList<GUIText>();
	private static int z = 0;
	private static int numberOfFPS = 0;
	private static FontType font;
	private static GUIText text;
	private static String textNumber = "0";
	
	
	public static void textSetup() {
		
		
		Loader loader = new Loader();
		if(numberOfFPS%(FPS_CAP/8 )== 0) {
		textNumber = Integer.toString(DisplayManager.MasterRefresh);
		}
		if(z == 0) {
			z = 1;
		font = new FontType(loader.loadTexture("candara"),"candara" );
		text = new GUIText(textNumber, 3f, font, new Vector2f(-0.45f, 0f), 1f, true);
		}

		texted.add(new GUIText(textNumber, 3f, font, new Vector2f(-0.45f, 0f), 1f, true));
		
		text.setColour(0, 0, 0);
		
		
		
	}
	
	
	public static void createDisplay(){		
		ContextAttribs attribs = new ContextAttribs(3,3)
		.withForwardCompatible(true)
		.withProfileCore(true);
		
		try {
			Display.setDisplayMode(new DisplayMode(WIDTH,HEIGHT));
			Display.create(new PixelFormat().withDepthBits(DEPHTS), attribs);
			Display.setTitle("Future And The Past");
			GL11.glEnable(GL13.GL_MULTISAMPLE);
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
		
		GL11.glViewport(0,0, WIDTH, HEIGHT);
		lastFrameTime = getCurrentTime();
	}
	

	
	
	
	private static void updateDisplay(){
		Display.sync(FPS_CAP);
		Display.update();
		currentFrameTime = getCurrentTime();
		delta = (currentFrameTime - lastFrameTime)/1000f;
		lastFrameTime = currentFrameTime;
		y++;

			
		texted.clear();

		isVisible();
		
		
	}
	public static void isVisible() {
		
		if(Keyboard.isKeyDown(Keyboard.KEY_BACK)&& y >= 30) {
			y = 0;
			i++;


		}
		if(i%2 !=0) {
		textSetup();
		}
	}
	public static void totalUpdate() {
		updateDisplay();
		FPS_counter();
		
	}
	public static int FPS_counter() {
		
		refresh =  delta * FPS_CAP;
		totalRefresh =  refresh ;
		MasterRefresh = (int) (FPS_CAP/totalRefresh);
		numberOfFPS += MasterRefresh;
//		System.out.println(MasterRefresh);
		return MasterRefresh;
	}
	
	
	public static float getFrameTimeSeconds(){
		return delta;
	}

	
	public static void closeDisplay(){
		Display.destroy();
	}
	
	public static long getCurrentTime(){
		return Sys.getTime()*1000/Sys.getTimerResolution();
	}


	public static long getLastFrameTime() {
		return lastFrameTime;
	}


	public static long getCurrentFrameTime() {
		return currentFrameTime;
	}
	public static Vector2f getNormalizedMouseCoordinates() {
		float normalizedX = -1.0f + 2.0f * (float) Mouse.getX() / (float) Display.getWidth();
		float normalizedY = 1.0f - 2.0f * (float) Mouse.getY() / (float) Display.getHeight();
		
//		System.out.println("X: " + normalizedX + ", Y: " + normalizedY);
		
		return new Vector2f(normalizedX, normalizedY);
	}
	

}
