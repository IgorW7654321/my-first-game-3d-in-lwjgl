package sunRenderer;

import shaders.ShaderProgram;

public class LightShader extends ShaderProgram{
	


	
	private static final String VERTEX_FILE = "/entities/LightVertex.glsl";
	private static final String FRAGMENT_FILE = "/entities/LightFragment.glsl";
	
	
	private int location_sunTexture;
	private int location_mvpMatrix;
	
	
	public LightShader() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}
	
	@Override
	protected void getAllUniformLocations() {
		
		location_sunTexture = super.getUniformLocation("sunTexture");
		location_mvpMatrix = super.getUniformLocation("mvpMatrix");
	}
	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
		super.bindAttribute(1, "textureCoordinates");
	}
	
public void connectTextureUnits() {
		super.loadInt(location_sunTexture, 6);
		super.loadInt(location_mvpMatrix, 7);
		
	}

}
