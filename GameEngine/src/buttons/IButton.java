package buttons;
 
import guis.GuiTexture;
 
import java.util.List;

public interface IButton {
    void onClick(IButton button);
 
    void whileHover(IButton button);
 
    void onStartHover(IButton button);
 
    void onStopHover(IButton button);
    
    void resetScale();
 
    void playHoverAnimation(float scaleFactor);
 
    void hide(List<GuiTexture> guiTextures);
 
    void show(List<GuiTexture> guiTextures);
}