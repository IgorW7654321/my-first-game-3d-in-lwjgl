package buttonsBackground;

import java.util.List;

import org.lwjgl.util.vector.Vector2f;

import guis.GuiTexture;
import renderEngine.DisplayManager;
import renderEngine.Loader;

public abstract class Background implements IBackground{

	    private Vector2f OriginalScale;
	    private GuiTexture guiTexture;
	    private boolean isHidden = false;
	    private boolean isHovering = false;
	    
		public Background(Loader loader,Vector2f position ,String texture, Vector2f scale) {
			guiTexture = new GuiTexture(loader.loadTexture(texture), position, scale);
			OriginalScale = scale;
			
		}
		
		public void update() {
			
			if(!isHidden) {
		    	Vector2f location = guiTexture.getPosition();
		    	Vector2f scale = guiTexture.getScale();
		    	Vector2f mouseCoordinates = DisplayManager.getNormalizedMouseCoordinates();
    	
		    		if(	   location.y + scale.y > -mouseCoordinates.y 
		    			&& location.y - scale.y < -mouseCoordinates.y
		    			&& location.x + scale.x > mouseCoordinates.x
		    			&& location.x - scale.x < mouseCoordinates.x) {
		    			
		    			whileHover(this);
		    		}
			
			}

		}
		
	    public void show(List<GuiTexture> guiTextureList) {
	    	if(isHidden) {
	    		guiTextureList.add(guiTexture);
	    		isHidden = false;
	    	}
	    }
	    	
	    	public void hide(List<GuiTexture> guiTextureList) {
	        	if(!isHidden) {
	        		guiTextureList.remove(guiTexture);
	        		isHidden = true;
	        	}
	    	}
	    	
	    	public void resetScale() {
	    		guiTexture.setScale(OriginalScale);
	    	}
	    	public boolean isHidden() {
				return isHidden;
			}
			public boolean isHovering() {
				return isHovering;
			}
			
			public void backgroundScale(float scaleFactorX, float scaleFactorY ) {
				guiTexture.setScale(OriginalScale.x + scaleFactorX, OriginalScale.y + scaleFactorY);
				
			}
   
}
