package buttonsBackground;

import java.util.List;

import guis.GuiTexture;

public interface IBackground {
	
	void whileHover(IBackground background);
	
	void hide(List<GuiTexture> guiTextures);
	 
    void show(List<GuiTexture> guiTextures);
    
    void resetScale();
}
