package models;

public class ModelTexture {
	
	private RawModel rawModel;
	private ModelTexture texture;

	
	public ModelTexture(RawModel model, ModelTexture texture){
		this.rawModel = model;
		this.texture = texture;
	}

	public RawModel getRawModel() {
		return rawModel;
	}

	public ModelTexture getTexture() {
		return texture;
	}

}
