package entities;

import org.lwjgl.util.vector.Vector3f;

import models.TexturedModel;
import skybox.SkyboxRenderer;

public class Light {
	private TexturedModel model;
	private float scale;
	public Vector3f position;
	private Vector3f colour;
	private Vector3f attenuation = new Vector3f(1, 0, 0);
	private int textureIndex = 0;
	private Vector3f lightDirection = new Vector3f(0, -1, 0);
	private static final float SUN_DIS = 50;
	
	public Light(Vector3f position, Vector3f colour) {
		this.position = position;
		this.colour = colour;

	}
	public Light(Vector3f position, Vector3f colour, Vector3f attenuation, TexturedModel model, float scale, int index) {
		this.position = position;
		this.colour = colour;
		this.attenuation = attenuation;
		this.model = model;
		this.scale = scale;
		this.textureIndex = index;
	}
	
	public Light(Vector3f position, Vector3f colour, TexturedModel model, float scale,int index) {
		this.position = position;
		this.colour = colour;
		this.model = model;
		this.scale = scale;
		this.textureIndex = index;
	}
	public Light(Vector3f position, Vector3f colour, Vector3f attenuation, TexturedModel model, float scale) {
		this.position = position;
		this.colour = colour;
		this.attenuation = attenuation;
		this.model = model;
		this.scale = scale;

	}
	
	public Light(Vector3f position, Vector3f colour, TexturedModel model, float scale) {
		this.position = position;
		this.colour = colour;
		this.model = model;
		this.scale = scale;

	}
	
	public Light(Vector3f position, Vector3f colour, Vector3f attenuation) {
		this.position = position;
		this.colour = colour;
		this.attenuation = attenuation;

	}
	
	public void movingPosition(double movingSpeed) {
			if(SkyboxRenderer.time <= 290000) {
			position.x += (32*movingSpeed);
			position.y += (16*movingSpeed);
//			System.out.println(SkyboxRenderer.time);
			}else if(SkyboxRenderer.time > 290000 && SkyboxRenderer.time < 550000){
			position.x += (32*movingSpeed);
			position.y -= (16*movingSpeed);
//			System.out.println(SkyboxRenderer.time);
			}else if(SkyboxRenderer.time > 290000 && SkyboxRenderer.time < 720000){
			position.x += (32*movingSpeed);
			position.y -= (16*movingSpeed);
//			System.out.println(SkyboxRenderer.time);
			}else if(SkyboxRenderer.time == 72000){
			position.x = -1000000;
			position.y = -1000;
//			System.out.println(SkyboxRenderer.time);
			
		
		}
	}
	public float getTextureXOffset(){
		int column = textureIndex%model.getTexture().getNumberOfRows();
		return (float)column/(float)model.getTexture().getNumberOfRows();
	}
	
	public float getTextureYOffset(){
		int row = textureIndex/model.getTexture().getNumberOfRows();
		return (float)row/(float)model.getTexture().getNumberOfRows();
	}
	
	public Vector3f getAttenuation(){
		return attenuation;
	}

	public Vector3f getPosition() {
		return position;
	}

	public void setPosition(Vector3f position) {
		this.position = position;
	}

	public Vector3f getColour() {
		return colour;
	}

	public void setColour(Vector3f colour) {
		this.colour = colour;
	}
	public TexturedModel getModel() {
		return model;
	}

	public void setModel(TexturedModel model) {
		this.model = model;
	}
	public float getScale() {
		return scale;
	}

	public void setScale(float scale) {
		this.scale = scale;
	}

	public Vector3f getWorldPosition(Vector3f camPos) {
		Vector3f sunPos = new Vector3f(lightDirection);
		sunPos.negate();
		sunPos.scale(SUN_DIS);
		return Vector3f.add(camPos, sunPos, null);
	}
	
	

}
