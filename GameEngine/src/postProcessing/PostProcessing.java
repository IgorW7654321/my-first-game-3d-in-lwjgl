package postProcessing;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import bloom.BrightFilter;
import bloom.CombineFilter;
import gaussianBlur.HorizontalBlur;
import gaussianBlur.VerticalBlur;
import models.RawModel;
import renderEngine.Loader;

public class PostProcessing {
	
	private static final float[] POSITIONS = { -1, 1, -1, -1, 1, 1, 1, -1 };	
	private static RawModel quad;
	private static ContrastChanger contrastChanger;
	private static HorizontalBlur hBlur;
	private static VerticalBlur vBlur;
	private static HorizontalBlur hBlur2;
	private static VerticalBlur vBlur2;
	private static HorizontalBlur hBlur1;
	private static VerticalBlur vBlur1;
	private static BrightFilter brightFilter;
	private static CombineFilter combineFilter;
	

	public static void init(Loader loader){
		quad = loader.loadToVAO(POSITIONS, 2);
		contrastChanger = new ContrastChanger();
		brightFilter = new BrightFilter(Display.getWidth() / 2, Display.getHeight() / 2);
		hBlur = new HorizontalBlur(Display.getWidth() / 8, Display.getHeight() / 8);
		vBlur = new VerticalBlur(Display.getWidth() / 8, Display.getHeight() / 8);
		hBlur1 = new HorizontalBlur(Display.getWidth() / 4, Display.getHeight() / 4);
		vBlur1 = new VerticalBlur(Display.getWidth() / 4, Display.getHeight() / 4);
		hBlur2 = new HorizontalBlur(Display.getWidth() / 2, Display.getHeight() / 2);
		vBlur2 = new VerticalBlur(Display.getWidth() / 2, Display.getHeight() / 2);
		combineFilter = new CombineFilter();
		/** blur efect = podzieli� ekran (tu na gorze) / 8 i zmieni� w shaderach*/
	}
	
	public static void doPostProcessing(int colourTexture, int brightTexture){
		start();
//		brightFilter.render(colourTexture);
//		hBlur.render(colourTexture);
//		vBlur.render(hBlur.getOutputTexture());
//		hBlur2.render(brightTexture);
//		vBlur2.render(hBlur2.getOutputTexture());
//		hBlur1.render(brightTexture);
//		vBlur1.render(hBlur2.getOutputTexture());
//		contrastChanger.render(vBlur2.getOutputTexture());
		contrastChanger.render(colourTexture);
//		combineFilter.render(colourTexture, vBlur2.getOutputTexture());
		end();
	}
	
	public static void cleanUp(){
		contrastChanger.cleanUp();
		hBlur.cleanUp();
		vBlur.cleanUp();
		hBlur2.cleanUp();
		vBlur2.cleanUp();
		brightFilter.cleanUp();
		combineFilter.cleanUp();
	}
	
	private static void start(){
		GL30.glBindVertexArray(quad.getVaoID());
		GL20.glEnableVertexAttribArray(0);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
	}
	
	private static void end(){
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL20.glDisableVertexAttribArray(0);
		GL30.glBindVertexArray(0);
	}


}
