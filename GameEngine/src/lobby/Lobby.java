package lobby;


import java.util.List;

import org.lwjgl.util.vector.Vector2f;

import guis.GuiTexture;
import renderEngine.Loader;

public class Lobby {
	private GuiTexture guiTexture;
	private final Vector2f scale = new Vector2f(1,1);
	private boolean isHidden = false;
    private boolean isHovering = false;
		
	
	public Lobby(Loader loader, String texture, Vector2f position) {
		guiTexture = new GuiTexture(loader.loadTexture(texture), position, scale);
		
	}

	public void show(List<GuiTexture> guiTextureList) {
		if(isHidden) {
			guiTextureList.add(guiTexture);
			isHidden = false;
	    }
	 }
	    	
	public void hide(List<GuiTexture> guiTextureList) {
		if(!isHidden) {
			guiTextureList.remove(guiTexture);
			isHidden = true;
	     }
	}
	
	public boolean isHidden() {
		return isHidden;
	}
	public boolean isHovering() {
		return isHovering;
	}
}