package sunRenderer;

import org.lwjgl.util.vector.Vector3f;

import textures.Texture;

public class Sun {

	private static final float SUN_DIS = 50;
											

	private final Texture texture;

	private Vector3f lightDirection = new Vector3f(0, -1, 0);
	private float scale;

	public Sun(Texture texture, float scale) {
		this.texture = texture;
		this.scale = scale;
	}

	public void setScale(float scale) {
		this.scale = scale;
	}

	public void setDirection(Vector3f dir) {
		lightDirection.set(dir);
		lightDirection.normalise();
	}

	public Texture getTexture() {
		return texture;
	}

	public Vector3f getLightDirection() {
		return lightDirection;
	}

	public float getScale() {
		return scale;
	}

	public Vector3f getWorldPosition(Vector3f camPos) {
		Vector3f sunPos = new Vector3f(lightDirection);
		sunPos.negate();
		sunPos.scale(SUN_DIS);
		return Vector3f.add(camPos, sunPos, null);
	}

}
