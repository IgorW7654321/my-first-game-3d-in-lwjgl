package loaders;

public class SceneLoaderFactory {

	public static SceneLoader createSceneLoader() {
		ModelLoader modelLoader = new ModelLoader();
		SkinLoader skinLoader = new SkinLoader();
		ConfigsLoader configsLoader = new ConfigsLoader();
		EntityLoader entityLoader = new EntityLoader(modelLoader, skinLoader, configsLoader);
		SkyboxLoader skyLoader = new SkyboxLoader();
		return new SceneLoader(entityLoader, skyLoader);
	}

}
