package lensFlare;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;

import openglObjects.Vao;
import utils.OpenGlUtils;


public class FlareRenderer {

	// 4 vertex positions for a 2D quad.
	private static final float[] POSITIONS = { -0.5f, -0.5f, -0.5f, 0.5f, 0.5f, -0.5f, 0.5f, 0.5f };

	// A VAO containing the quad's positions in attribute 0
	private final Vao quad;
	private final FlareShader shader;


	public FlareRenderer() {
		this.shader = new FlareShader();
		this.quad = Vao.create();
		quad.bind();
		quad.storeData(4, POSITIONS);
		quad.unbind();
	}


	public void render(FlareTexture[] flares, float brightness) {
		prepare(brightness);
		for (FlareTexture flare : flares) {
			renderFlare(flare);
		}
		endRendering();
	}

	
	public void cleanUp() {
		shader.cleanUp();
	}


	private void prepare(float brightness) {
		OpenGlUtils.antialias(false);
		OpenGlUtils.enableAdditiveBlending();
		OpenGlUtils.enableDepthTesting(false);
		OpenGlUtils.cullBackFaces(false);
		shader.start();
		shader.brightness.loadFloat(brightness);
		quad.bind(0);
	}


	private void renderFlare(FlareTexture flare) {
		flare.getTexture().bindToUnit(0);
		float xScale = flare.getScale();
		float yScale = xScale * (float) Display.getWidth() / Display.getHeight();
		Vector2f centerPos = flare.getScreenPos();
		shader.transform.loadVec4(centerPos.x, centerPos.y, xScale, yScale);
		GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, 4);
	}

	private void endRendering() {
		quad.unbind(0);
		shader.stop();
		OpenGlUtils.disableBlending();
		OpenGlUtils.enableDepthTesting(true);
		OpenGlUtils.cullBackFaces(true);
	}

}
