package Snake;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.Timer;
import java.awt.Color;
import java.nio.*;
import javax.swing.*;
import static com.jogamp.opengl.GL4.*;
import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.common.nio.Buffers;

public class Snake extends JFrame implements ActionListener, KeyListener,GLEventListener
{

	
    public static Snake snake;
 
    public JFrame jframe;
    

 
    public Render renderPanel;    
    
	public Timer timer= new Timer(15, this);
	
	public Timer timerA= new Timer(1000, this);
	
    public static ArrayList<Point> snakeParts = new ArrayList<Point>();
 
    public static final int UP = 0, DOWN = 1, LEFT = 2, RIGHT = 3, SCALE = 10;
 
    public static int ticks = 0, direction = DOWN, score, tailLength = 10, time;
 
    public static Point head, cherry;
 
    public Random random;
    
    static String NickFromK;
 
    public static boolean over = false, paused;
 
    public static Dimension dim;
    
    public static int level = 1;
    
    public int PN;
    
    public static int timem;
    
    public static int timeh;
    
    public static String NameOfAchievement;
    
    public int LC;
    
    public int PN1;
    
    private GLCanvas myCanvas;
    
    public void Check() {
    
    	PN1 = PN1 + 30 ;
    	LC++;
    	if(LC>level) {
    		level = LC;
    		
    	}
    	
    }

    
    
 
    public Snake()
    {
    	
    	SnakeWindow();
        startGame();
    }
   
    
    public void SnakeWindow() {
        dim = Toolkit.getDefaultToolkit().getScreenSize();
        setTitle("Chapter2 - program1");
    	setLocation(200, 200);
    	myCanvas = new GLCanvas();
    	myCanvas.addGLEventListener(this);
    	this.add(renderPanel);
    	setVisible(true);
    }
 




	public void startGame()
    {
        over = false;
        paused = false;
        time = 0;
        timem = 0;
        timeh = 0;
        score = 0;
        tailLength = 14;
        level = 1;
        LC = 1;
        PN = 30;
        PN1 = 30;
        ticks = 0;
        direction = DOWN;
        head = new Point(0, -1);
        random = new Random();
        snakeParts.clear();
        cherry = new Point(random.nextInt(79), random.nextInt(66));
        timer.start();
    }
		
 
    @Override
    public void actionPerformed(ActionEvent arg0)
    {

        renderPanel.repaint();
        ticks++;
 
        if (ticks % 2 == 0 && head != null && !over && !paused)
        {
            time++;
            if(time==60*27) {
            	time = 0;
            	timem++;
            }if(timem == 60 ) {
            	timem = 0;
            	timeh++;
            }
            
            if(score>=PN) {
            	PN=PN+30;
            	level++;
            }

 
            snakeParts.add(new Point(head.x, head.y));
 
            if (direction == UP)
            {
                if (head.y - 1 >= 0 && noTailAt(head.x, head.y - 1))
                {
                    head = new Point(head.x, head.y - 1);
                }
                else
                {
                    over = true;
 
                }
            }
 
            if (direction == DOWN)
            {
                if (head.y + 1 < 67 && noTailAt(head.x, head.y + 1))
                {
                    head = new Point(head.x, head.y + 1);
                }
                else
                {
                    over = true;
                }
            }
 
            if (direction == LEFT)
            {
                if (head.x - 1 >= 0 && noTailAt(head.x - 1, head.y))
                {
                    head = new Point(head.x - 1, head.y);
                }
                else
                {
                    over = true;
                }
            }
 
            if (direction == RIGHT)
            {
                if (head.x + 1 < 80 && noTailAt(head.x + 1, head.y))
                {
                    head = new Point(head.x + 1, head.y);
                }
                else
                {
                    over = true;
                }
            }
 
            
            
            if (snakeParts.size() > tailLength)
            {
                snakeParts.remove(0);
 
            }
 
            if (cherry != null)
            {
                if (head.equals(cherry))
                {
                    score += 10;
                    tailLength++;
                    cherry.setLocation(random.nextInt(79), random.nextInt(66));
                }
            }
        }
    }
 
    public boolean noTailAt(int x, int y)
    {
        for (Point point : snakeParts)
        {
            if (point.equals(new Point(x, y)))
            {
                return false;
            }
        }
        return true;
    }
 

    @Override
    public void keyPressed(KeyEvent e)
    {
        int i = e.getKeyCode();
 
        if ((i == KeyEvent.VK_A || i == KeyEvent.VK_LEFT) && direction != RIGHT)
        {
            direction = LEFT;
        }
 
        if ((i == KeyEvent.VK_D || i == KeyEvent.VK_RIGHT) && direction != LEFT)
        {
            direction = RIGHT;
        }
 
        if ((i == KeyEvent.VK_W || i == KeyEvent.VK_UP) && direction != DOWN)
        {
            direction = UP;
        }
 
        if ((i == KeyEvent.VK_S || i == KeyEvent.VK_DOWN) && direction != UP)
        {
            direction = DOWN;
        }
 
        if (i == KeyEvent.VK_SPACE)
        {
            if (over)
            {
                startGame();
            }
            else
            {
                paused = !paused;
            }
        }
    }
    
 
    @Override
    public void keyReleased(KeyEvent e)
    {
    }
 
    @Override
    public void keyTyped(KeyEvent e){
    	
    	
    }




	@Override
	public void display(GLAutoDrawable arg0) {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void dispose(GLAutoDrawable arg0) {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void init(GLAutoDrawable arg0) {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void reshape(GLAutoDrawable arg0, int arg1, int arg2, int arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}
    }