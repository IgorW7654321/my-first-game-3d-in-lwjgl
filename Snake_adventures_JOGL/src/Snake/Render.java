package Snake;

import java.awt.Color;

import java.awt.Graphics;

import java.awt.Point;
 
import javax.swing.JPanel;
 
@SuppressWarnings("serial")
public class Render extends JPanel
{
 
    public static final Color GREEN = new Color(1666073);
    public static final Color BLACK = new Color(000);
    public static final Color DARKGREEN = new Color(01000);
    public static final Color PURPLE = new Color(1280128);
    public static final Color MEDIUMSLATEBLUE = new Color(123104238);
    public static final Color GOLD = new Color(2552150);
    public static final Color OLIVE = new Color(1281280);
    public static final Color SILVER = new Color(192192192);
    public static final Color BLUE = new Color(00255);
    public static final Color CYAN = new Color(0255255);
    public static final Color WHITE = new Color(255255255);
 
    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
       
        Snake snake = Snake.snake;
        
        if(Snake.level>0 && Snake.level<5) {
        	g.setColor(GREEN);
        	g.fillRect(0, 0, 800, 700);
        }else if(Snake.level>4 && Snake.level<10) {
        	g.setColor(SILVER);
        	g.fillRect(0, 0, 800, 700);
        }else if(Snake.level>9 && Snake.level<15) {
        	g.setColor(BLACK);
        	g.fillRect(0, 0, 800, 700);
        }else if(Snake.level>14 && Snake.level<20) {
        	g.setColor(PURPLE);
        	g.fillRect(0, 0, 800, 700);
        }else if(Snake.level>19 && Snake.level<25) {
        	g.setColor(BLACK);
        	g.fillRect(0, 0, 800, 700);
        }else if(Snake.level>24){
        	g.setColor(GREEN);
        	g.fillRect(0, 0, 800, 700);
        }

        g.fillRect(0, 0, 800, 700);
        
        if(Snake.level>0 && Snake.level<5) {
        	g.setColor(BLUE);
        
        }else if(Snake.level>4 && Snake.level<10) {
        	g.setColor(GREEN);
        	
        }else if(Snake.level>9 && Snake.level<15) {
        	g.setColor(SILVER);
        	
        }else if(Snake.level>14 && Snake.level<20) {
        	g.setColor(BLACK);
        }else if(Snake.level>19 && Snake.level<25) {
        	g.setColor(PURPLE);
        }else if(Snake.level>24 && Snake.level<30){
        	g.setColor(BLUE);
        }
        
        for (Point point : Snake.snakeParts)
        {
            g.fillRect(point.x * Snake.SCALE, point.y * Snake.SCALE, Snake.SCALE, Snake.SCALE);
        }
        
        
       
        if(Snake.score == 110 && Snake.level == 4 || Snake.score == 260 && Snake.level == 9 || Snake.score == 410 && Snake.level == 14 || Snake.score == 560 && Snake.level == 19 || Snake.score == 710 && Snake.level == 24 || Snake.score == 860 && Snake.level == 29    ) {
        	g.fillRect(snake.cherry.x * Snake.SCALE, snake.cherry.y * Snake.SCALE, Snake.SCALE, Snake.SCALE);
        g.setColor(Color.BLUE);
  
        }else {
        	g.fillRect(Snake.cherry.x * Snake.SCALE, Snake.cherry.y * Snake.SCALE, Snake.SCALE, Snake.SCALE);
             g.setColor(Color.RED);
 
        }
        g.fillRect(Snake.cherry.x * Snake.SCALE, Snake.cherry.y * Snake.SCALE, Snake.SCALE, Snake.SCALE);
       
        String Label = "Score: " + Snake.score + ", Length: " + Snake.tailLength +", LEVEL: " +Snake.level +"   , Hours: " +Snake.timeh+ ", Minutes: "+Snake.timem+", Secounds: " + Snake.time/27;
       
        g.setColor(Color.white);
       
        g.drawString(Label, (int) (getWidth() / 2 - Label.length() * 2.5f), 10);
 
        Label = "Game Over!         Press space";
 
        if (Snake.over)
        {
          g.drawString(Label, (int) (getWidth() / 2 - Label.length() * 2.5f), (int) Snake.dim.getHeight() / 4);
        }
 
        Label= "Paused!";
 
        if (Snake.paused && !Snake.over)
        {
            g.drawString(Label, (int) (getWidth() / 2 - Label.length() * 2.5f), (int) Snake.dim.getHeight() / 4);
        }
    }
}