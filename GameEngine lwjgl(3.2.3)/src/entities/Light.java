package entities;


import org.joml.Vector3f;

import skybox.SkyboxRenderer;

public class Light {
	
	public Vector3f position;
	private Vector3f colour;
	private Vector3f attenuation = new Vector3f(1, 0, 0);
	
	public Light(Vector3f position, Vector3f colour) {
		this.position = position;
		this.colour = colour;

	}
	
	public Light(Vector3f position, Vector3f colour, Vector3f attenuation) {
		this.position = position;
		this.colour = colour;
		this.attenuation = attenuation;

	}
	
	public void movingPosition(double movingSpeed) {
			if(SkyboxRenderer.time <= 290000) {
			position.x += (32*movingSpeed);
			position.y += (16*movingSpeed);
//			System.out.println(SkyboxRenderer.time);
			}else if(SkyboxRenderer.time > 290000 && SkyboxRenderer.time < 550000){
			position.x += (32*movingSpeed);
			position.y -= (16*movingSpeed);
//			System.out.println(SkyboxRenderer.time);
			}else if(SkyboxRenderer.time > 290000 && SkyboxRenderer.time < 720000){
			position.x += (32*movingSpeed);
			position.y -= (16*movingSpeed);
//			System.out.println(SkyboxRenderer.time);
			}else if(SkyboxRenderer.time == 72000){
			position.x = -1000000;
			position.y = -1000;
//			System.out.println(SkyboxRenderer.time);
			
		
		}
	}
	
	public Vector3f getAttenuation(){
		return attenuation;
	}

	public Vector3f getPosition() {
		return position;
	}

	public void setPosition(Vector3f position) {
		this.position = position;
	}

	public Vector3f getColour() {
		return colour;
	}

	public void setColour(Vector3f colour) {
		this.colour = colour;
	}

	
	

}
