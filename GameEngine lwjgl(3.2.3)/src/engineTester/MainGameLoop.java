package engineTester;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;


import entities.Camera;
import entities.Entity;
import entities.Light;
import entities.Player;
import fontMeshCreator.FontType;
import fontMeshCreator.GUIText;
import fontRendering.TextMaster;
import guis.GuiRenderer;
import guis.GuiTexture;
import models.RawModel;
import models.TexturedModel;
import normalMappingObjConverter.NormalMappedObjLoader;
import objConverter.OBJFileLoader;
import particles.Particle;
import particles.ParticleMaster;
import particles.ParticleSystem;
import particles.ParticleTexture;
import postProcessing.Fbo;
import postProcessing.PostProcessing;
import renderEngine.DisplayManager;
import renderEngine.Loader;
import renderEngine.MasterRenderer;
import renderEngine.OBJLoader;
import skybox.SkyboxRenderer;
import terrains.Terrain;
import textures.ModelTexture;
import textures.TerrainTexture;
import textures.TerrainTexturePack;
import toolbox.MousePicker;
import water.WaterFrameBuffers;
import water.WaterRenderer;
import water.WaterShader;
import water.WaterTile;

public class MainGameLoop {
	public static void main(String[] args) {
		
		

		DisplayManager.createDisplay();
		Loader loader = new Loader();
		TextMaster.init(loader);
		TexturedModel playerModel = new TexturedModel(OBJLoader.loadObjModel("person", loader), new ModelTexture(
				loader.loadTexture("playerTexture")));

		Player player = new Player(playerModel, new Vector3f(75, 5, -75), 0, 100, 0, 0.6f);
		
		Camera camera = new Camera(player);
		MasterRenderer renderer = new MasterRenderer(loader, camera);
		ParticleMaster.init(loader, renderer.getProjectionMatrix());

		// *********TERRAIN TEXTURE STUFF**********
		
		TerrainTexture backgroundTexture = new TerrainTexture(loader.loadTexture("grassy2"));
		TerrainTexture rTexture = new TerrainTexture(loader.loadTexture("mud"));
		TerrainTexture gTexture = new TerrainTexture(loader.loadTexture("grassFlowers"));
		TerrainTexture bTexture = new TerrainTexture(loader.loadTexture("path"));

		TerrainTexturePack texturePack = new TerrainTexturePack(backgroundTexture, rTexture,
				gTexture, bTexture);
		TerrainTexture blendMap = new TerrainTexture(loader.loadTexture("blendMap"));

		// *****************************************


		
		ModelTexture fernTextureAtlas = new ModelTexture(loader.loadTexture("fern"));
		fernTextureAtlas.setNumberOfRows(2);

		TexturedModel fern = new TexturedModel(OBJFileLoader.loadOBJ("fern", loader),
				fernTextureAtlas);

		TexturedModel bobble = new TexturedModel(OBJFileLoader.loadOBJ("pine", loader),
				new ModelTexture(loader.loadTexture("pine")));
		bobble.getTexture().setHasTransparency(true);

		fern.getTexture().setHasTransparency(true);
		
		TexturedModel lamp = new TexturedModel(OBJLoader.loadObjModel("lamp", loader),
				new ModelTexture(loader.loadTexture("lamp")));
		lamp.getTexture().setUseFakeLighting(true);
		
		TexturedModel cherryModel = new TexturedModel(OBJLoader.loadObjModel("cherry", loader),
				new ModelTexture(loader.loadTexture("cherry")));
		cherryModel.getTexture().setHasTransparency(true);
		cherryModel.getTexture().setShineDamper(10);
		cherryModel.getTexture().setReflectivity(0.5f);
		cherryModel.getTexture().setSpecularMap(loader.loadTexture("cherryS"));
		
		TexturedModel lantern = new TexturedModel(OBJLoader.loadObjModel("lantern", loader),
				new ModelTexture(loader.loadTexture("lantern")));
		lantern.getTexture().setSpecularMap(loader.loadTexture("lanternS"));
		
		//******************TERRAIN SETUP******************

		Terrain terrain = new Terrain(0, -1, loader, texturePack, blendMap, "heightmap");

		List<Terrain> terrains = new ArrayList<Terrain>();
		terrains.add(terrain);

		List<Entity> entities = new ArrayList<Entity>();
		List<Entity> normalMapEntities = new ArrayList<Entity>();
		
		//******************NORMAL MAP MODELS************************
		
		TexturedModel barrelModel = new TexturedModel(NormalMappedObjLoader.loadOBJ("barrel", loader),
				new ModelTexture(loader.loadTexture("barrel")));
		barrelModel.getTexture().setNormalMap(loader.loadTexture("barrelNormal"));
		barrelModel.getTexture().setShineDamper(10);
		barrelModel.getTexture().setReflectivity(0.5f);
		
		TexturedModel crateModel = new TexturedModel(NormalMappedObjLoader.loadOBJ("crate", loader),
				new ModelTexture(loader.loadTexture("crate")));
		crateModel.getTexture().setNormalMap(loader.loadTexture("crateNormal"));
		crateModel.getTexture().setShineDamper(10);
		crateModel.getTexture().setReflectivity(0.5f);
		
		TexturedModel boulderModel = new TexturedModel(NormalMappedObjLoader.loadOBJ("boulder", loader),
				new ModelTexture(loader.loadTexture("boulder")));
		boulderModel.getTexture().setNormalMap(loader.loadTexture("boulderNormal"));
		boulderModel.getTexture().setShineDamper(10);
		boulderModel.getTexture().setReflectivity(0.5f);
		
		
		//************ENTITIES*******************
		
//		Entity entity = new Entity(barrelModel, new Vector3f(75, 10, -75), 0, 0, 0, 1f);
//		Entity entity2 = new Entity(boulderModel, new Vector3f(85, 10, -75), 0, 0, 0, 1f);
//		Entity entity3 = new Entity(crateModel, new Vector3f(65, 10, -75), 0, 0, 0, 0.04f);
//		normalMapEntities.add(entity);
//		normalMapEntities.add(entity2);
//		normalMapEntities.add(entity3);
		
		Random random = new Random(5666778);
		for (int i = 0; i < 600; i++) {
			
			if (i % 12 == 0) {
				float x = random.nextFloat() * 800;
				float z = random.nextFloat() * -800;
				float y = terrain.getHeightOfTerrain(x, z);
				
				entities.add(new Entity(cherryModel, 1, new Vector3f(x, y, z), 0,
						random.nextFloat() * 360, 0, random.nextFloat() * 0.6f + 1.8f));
		
			}
			

//			if (i % 6 == 0) {
//				float x = random.nextFloat() * 800;
//				float z = random.nextFloat() * -800;
//				float y = terrain.getHeightOfTerrain(x, z);
//				
//				entities.add(new Entity(boulderModel, 1, new Vector3f(x, y, z), 0,
//						random.nextFloat() * 360, 0, random.nextFloat() * 0.6f + 1.8f));
	//	
//				entities.add(new Entity(lantern, 1, new Vector3f(x, y, z), 0,
//						random.nextFloat() * 360, 0, random.nextFloat() * 0.6f + 1.8f));
	//	
//				
//			}
			if (i % 3 == 0) {
				float x = random.nextFloat() * 800;
				float z = random.nextFloat() * -800;
				float y = terrain.getHeightOfTerrain(x, z);

					
					entities.add(new Entity(bobble, 1, new Vector3f(x, y, z), 0,
							random.nextFloat() * 360, 0, random.nextFloat() * 0.3f + 2.0f));
				
			
			

			}
			if (i % 2 == 0) {

				float x = random.nextFloat() * 800;
				float z = random.nextFloat() * -800;
				float y = terrain.getHeightOfTerrain(x, z);
					
				entities.add(new Entity(fern,random.nextInt(1+4), new Vector3f(x, y, z), 0,
						random.nextFloat() * 360, 0, 0.9f));
			
				
				
				
				
			}
		}
		
		//*******************OTHER SETUP***************

		List<Light> lights = new ArrayList<Light>();
		Light sun = new Light(new Vector3f(-1000000, -1000, 0), new Vector3f(1.3f, 1.3f, 1.3f));
		lights.add(sun);
		
		

		entities.add(player);
		MousePicker picker = new MousePicker(camera, renderer.getProjectionMatrix(), terrain);


		

		RawModel playerOBJ = OBJLoader.loadObjModel("person", loader);
		//**********GUIs*********
		
		List<GuiTexture> guiTextures = new ArrayList<GuiTexture>();
		GuiTexture shadowMap = new GuiTexture(renderer.getShadowMapTexture(),
				new Vector2f(0.5f, 0.5f), new Vector2f(0.5f, 0.5f));
		//guiTextures.add(shadowMap);
		GuiRenderer guiRenderer = new GuiRenderer(loader);
		
		//**********Water Renderer Set-up************************
		
		
		WaterFrameBuffers buffers = new WaterFrameBuffers();
		WaterShader waterShader = new WaterShader();
		WaterRenderer waterRenderer = new WaterRenderer(loader, waterShader, renderer.getProjectionMatrix(), buffers);
		List<WaterTile> waters = new ArrayList<WaterTile>();
		WaterTile water = new WaterTile(75, -75, 0);
		waters.add(water);
		//****************PARTICLE SYSTEM*********************
//		ParticleTexture particleTexture = new ParticleTexture(loader.loadTexture("particleStar"), 1);
	//	
//		ParticleSystem system = new ParticleSystem(particleTexture,40 ,10 ,0.1f ,5 ,1.6f);
//		system.randomizeRotation();
//		system.setDirection(new Vector3f(1,0,1), 0,1f);
//		system.setLifeError(0.1f);
//		system.setSpeedError(0.4f);
//		system.setScaleError(0.8f);
//		system.setColour(0.25f, 0.015f, 0.0f);
		
		//***************FBO SET-UP*************
		
		Fbo multisampleFbo = new Fbo(Display.getWidth(), Display.getHeight());
		Fbo outputFbo = new Fbo(Display.getWidth(), Display.getHeight(), Fbo.DEPTH_TEXTURE);
		Fbo outputFbo2 = new Fbo(Display.getWidth(), Display.getHeight(), Fbo.DEPTH_TEXTURE);
		PostProcessing.init(loader);
		
		
		//****************Game Loop Below*********************
			
			while (!Display.isCloseRequested()) {
				
				
				 for(Terrain terr : terrains) {
					    if(terrain.getX() <= player.getPosition().x) { 
					     if(terrain.getX() + Terrain.getSize() > player.getPosition().x) {
					      if(terrain.getZ() <= player.getPosition().z) {
					       if(terrain.getZ() + Terrain.getSize() > player.getPosition().z) {
					        player.move(terrain);
					       }
					      }
					     }
					    }
					   }
			camera.move();
			picker.update();
			if(player.PlayerMove) {
//				system.generateParticles(player.getPosition());
			}

			sun.movingPosition(3f);
			
			
//			system.generateParticles(new Vector3f(490, 40 ,-300));
			
			
			renderer.renderShadowMap(entities, sun);
			
			ParticleMaster.update(camera);
//			entity.increaseRotation(0, 1, 0);
//			entity2.increaseRotation(0, 1, 0);
//			entity3.increaseRotation(0, 1, 0);
			GL11.glEnable(GL30.GL_CLIP_DISTANCE0);
			
			//render reflection texture
			buffers.bindReflectionFrameBuffer();
			float distance = 2 * (camera.getPosition().y - water.getHeight());
			camera.getPosition().y -= distance;
			camera.invertPitch();
			renderer.renderScene(entities, normalMapEntities, terrains, lights, camera, new Vector4f(0, 1, 0, -water.getHeight()+1));
			camera.getPosition().y += distance;
			camera.invertPitch();
			
			//render refraction texture
			buffers.bindRefractionFrameBuffer();
			renderer.renderScene(entities, normalMapEntities, terrains, lights, camera, new Vector4f(0, -1, 0, water.getHeight()));
//			
			//render to screen
			GL11.glDisable(GL30.GL_CLIP_DISTANCE0);
			buffers.unbindCurrentFrameBuffer();	
			
			multisampleFbo.bindFrameBuffer();
			renderer.renderScene(entities, normalMapEntities, terrains, lights, camera, new Vector4f(0, -1, 0, 100000));	
			waterRenderer.render(waters, camera, sun);
			ParticleMaster.renderParticles(camera);
			multisampleFbo.unbindFrameBuffer();
			multisampleFbo.resolveToFbo(GL30.GL_COLOR_ATTACHMENT0  ,outputFbo);
			multisampleFbo.resolveToFbo(GL30.GL_COLOR_ATTACHMENT1  ,outputFbo2);
			PostProcessing.doPostProcessing(outputFbo.getColourTexture(), outputFbo2.getColourTexture());
			
			guiRenderer.render(guiTextures);
			TextMaster.render();
			DisplayManager.totalUpdate();

		}

		//*********Clean Up Below**************
		
		PostProcessing.cleanUp();
		multisampleFbo.cleanUp();
		outputFbo.cleanUp();
		ParticleMaster.cleanUp();
		TextMaster.cleanUp();
		outputFbo2.cleanUp();
		buffers.cleanUp();
		waterShader.cleanUp();
		guiRenderer.cleanUp();
		renderer.cleanUp();
		loader.cleanUp();
		
		DisplayManager.closeDisplay();

		
	}
	


}

